package presentacion.controlador;

import java.util.UUID;

import dto.ContactoDTO;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import modelo.GestorContactos;

public class ControladorVentanaAgregarEditarContacto 
{

	@FXML
	private VBox panelAgregarEditar;

	@FXML
	private TextField txtNombre;
	@FXML
	private TextArea txtDescripcion;
	@FXML
	private TextField txtTelefono;
	@FXML
	private TextField txtDireccion;
	@FXML
	private TextArea txtOtrosMedios;
	@FXML
	private TextField txtReferencia;
	
	@FXML
	private Label lblNombre;
	@FXML
	private Label lblTelefono;
	@FXML
	private Label lblDireccion;	
	
	@FXML
	private Button btnGuardar;

	public ContactoDTO contacto;
	public String tipo;
	
	public ControladorVentanaPrincipal contro;
	
	public void initialize(String tipo, ContactoDTO contacto, ControladorVentanaPrincipal contro)
	{	
		this.tipo = tipo;
		this.contacto = contacto;
		this.contro = contro;
		
		this.contro.btnAgregar.setDisable(true);
		this.contro.btnEditar.setDisable(true);
		this.contro.btnEliminar.setDisable(true);
		this.contro.MenuButtonBD.setDisable(true);
		
		if (tipo.equals("agregar"))
		{ 			
			this.panelAgregarEditar.setStyle("-fx-background-image: url('resources/images/fondoAgregar.jpg'); -fx-background-size: cover; ");
		}
		else if (tipo.equals("editar"))
		{ 
			this.panelAgregarEditar.setStyle("-fx-background-image: url('resources/images/fondoEditar.jpg'); -fx-background-size: cover; ");
			
			this.txtNombre.setText(this.contacto.getNombre());
			this.txtDescripcion.setText(this.contacto.getDescripcion());
			this.txtTelefono.setText(this.contacto.getTelefono());
			this.txtDireccion.setText(this.contacto.getDireccion());
			this.txtOtrosMedios.setText(this.contacto.getOtrosmedios());
			this.txtReferencia.setText(this.contacto.getReferencia());
		}
		
		Image img1 = new Image("resources/images/nombre.png");
	    ImageView view1 = new ImageView(img1);
	    view1.setFitHeight(20);
	    view1.setFitWidth(20);
	    this.lblNombre.setGraphic(view1);
	    
		Image img2 = new Image("resources/images/telefono.png");
	    ImageView view2 = new ImageView(img2);
	    view2.setFitHeight(20);
	    view2.setFitWidth(20);
	    this.lblTelefono.setGraphic(view2);
	    
	    Image img3 = new Image("resources/images/direccion.png");
	    ImageView view3 = new ImageView(img3);
	    view3.setFitHeight(20);
	    view3.setFitWidth(20);
	    this.lblDireccion.setGraphic(view3);
	}	
	
	@FXML
	public void guardar(ActionEvent event) 
	{
		if ( this.tipo.equals("agregar") )
		{
			ContactoDTO newContact = new ContactoDTO();
			newContact.setIdContacto(UUID.randomUUID().toString());
			newContact.setNombre( this.txtNombre.getText() );
			newContact.setDescripcion( this.txtDescripcion.getText() );
			newContact.setTelefono( this.txtTelefono.getText() );
			newContact.setDireccion( this.txtDireccion.getText() );
			newContact.setOtrosmedios( this.txtOtrosMedios.getText() );
			newContact.setReferencia( this.txtReferencia.getText() );

			GestorContactos.getInstance().insert(newContact);
		}
		else if ( this.tipo.equals("editar") )
		{
			this.contacto.setNombre(this.txtNombre.getText());
			this.contacto.setDescripcion(this.txtDescripcion.getText());
			this.contacto.setTelefono(this.txtTelefono.getText());
			this.contacto.setDireccion(this.txtDireccion.getText());
			this.contacto.setOtrosmedios(this.txtOtrosMedios.getText());
			this.contacto.setReferencia(this.txtReferencia.getText());

			GestorContactos.getInstance().update(contacto);			
		}

		this.contro.contactos.clear();
		this.contro.contactosFiltrados.clear();
		this.contro.contactos = FXCollections.observableArrayList( GestorContactos.getInstance().readAll() );
		this.contro.contactosFiltrados = FXCollections.observableArrayList( GestorContactos.getInstance().readAll() );
		
		this.contro.tblContactos.setItems(this.contro.contactosFiltrados);
		this.contro.tblContactos.refresh();
		
		Platform.runLater( () -> this.contro.tblContactos.scrollTo(this.contro.contactos.size()-1) );
		
		this.cerrar();
	}
	
	@FXML
	public void cerrar()
	{
		this.contro.txtFiltro.setText("");
		this.contro.btnAgregar.setDisable(false);
		this.contro.MenuButtonBD.setDisable(false);
		this.contro.borderPaneVentanaPrincipal.setLeft( null );
	}
	
}