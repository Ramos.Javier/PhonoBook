package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.ContactoDTO;
import persistencia.dao.interfaz.ContactoDAO;
import persistencia.conexion.Conexion;

public class ContactoDAOMYSQL implements ContactoDAO
{
	
	private static ContactoDAOMYSQL instance;
	private static final String insert = "INSERT INTO contacto (idContacto, nombre, descripcion, telefono, direccion, otrosMedios, referencia) VALUES(?, ?, ?, ?, ?, ?, ?)";
	private static final String update = "UPDATE contacto SET nombre=?, descripcion=?, telefono=?, direccion=?, otrosMedios=?, referencia=? WHERE idContacto=?";
	private static final String delete = "DELETE FROM contacto WHERE idContacto=?";
	private static final String readAll = "SELECT * FROM contacto";
	private static final String readForId = "SELECT * FROM contacto WHERE idContacto=?";
	
	public static ContactoDAOMYSQL getInstance()
	{
		if (instance == null)
			instance = new ContactoDAOMYSQL();
		return instance;
	}
	
	@Override
	public boolean insert(ContactoDTO contacto) 
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{	
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setString(1, contacto.getIdContacto());
			statement.setString(2, contacto.getNombre());
			statement.setString(3, contacto.getDescripcion());
			statement.setString(4, contacto.getTelefono());
			statement.setString(5, contacto.getDireccion());
			statement.setString(6, contacto.getOtrosmedios());
			statement.setString(7, contacto.getReferencia());
			if(statement.executeUpdate() > 0) { return true; }
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(ContactoDTO contacto) 
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, contacto.getNombre());
			statement.setString(2, contacto.getDescripcion());
			statement.setString(3, contacto.getTelefono());
			statement.setString(4, contacto.getDireccion());
			statement.setString(5, contacto.getOtrosmedios());
			statement.setString(6, contacto.getReferencia());
			statement.setString(7, contacto.getIdContacto());
			if(statement.executeUpdate() > 0)
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(ContactoDTO contacto) 
	{
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, contacto.getIdContacto());
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) 
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<ContactoDTO> readAll() 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<ContactoDTO> contactos = new ArrayList<ContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAll);
			resultSet = statement.executeQuery();			
			while(resultSet.next())
			{
				ContactoDTO newContacto = new ContactoDTO();
				newContacto.setIdContacto(resultSet.getString("idContacto"));				
				newContacto.setNombre(resultSet.getString("nombre"));
				newContacto.setDescripcion(resultSet.getString("descripcion"));
				newContacto.setTelefono(resultSet.getString("telefono"));
				newContacto.setDireccion(resultSet.getString("direccion"));
				newContacto.setOtrosmedios(resultSet.getString("otrosMedios"));
				newContacto.setReferencia(resultSet.getString("referencia"));
				contactos.add(newContacto);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return contactos;
	}

	@Override
	public ContactoDTO readForId(int idContacto) 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		ContactoDTO contacto = new ContactoDTO(); 
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idContacto);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				contacto.setIdContacto(resultSet.getString("idContacto"));				
				contacto.setNombre(resultSet.getString("nombre"));
				contacto.setDescripcion(resultSet.getString("descripcion"));
				contacto.setTelefono(resultSet.getString("telefono"));
				contacto.setDireccion(resultSet.getString("direccion"));
				contacto.setOtrosmedios(resultSet.getString("otrosMedios"));
				contacto.setReferencia(resultSet.getString("referencia"));
			}	
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return contacto;
	}

}